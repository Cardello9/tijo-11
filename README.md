Kompozyt (wzorzec projektowy)

Kompozyt – strukturalny wzorzec projektowy, którego celem jest składanie obiektów w taki sposób, aby klient widział wiele z nich jako pojedynczy obiekt.

Przykładowe zastosowanie:
Wzorzec ten stosuje się, gdy wygodniej jest korzystać z pewnych operacji dla danego obiektu w ten sam sposób jak dla grupy obiektów, np. 
rysując na ekranie prymitywy lub obiekty złożone z prymitywów; zmieniając rozmiar zarówno pojedynczych prymitywów jak i obiektów złożonych z prymitywów (z zachowaniem proporcji).

Struktura wzorca:
Wzorzec wyróżnia następujące elementy:

Component - klasa abstrakcyjna reprezentująca pojedyncze obiekty Leaf, jak i kontenery tych obiektów.

Leaf - typ prosty - nie posiada potomków.

Composite - przechowuje obiekty proste (Leaf), implementuje zachowanie elementów które zawiera.

Composite jak i Leaf dziedziczy po tym samym interfejsie co pozwala na dostęp do obiektów prostych w ten sam sposób jak do grupy tych obiektów. 
Użytkownik może przeprowadzać operacje na pojedynczym obiekcie, jak i na grupie obiektów reprezentowanych tym wzorcem. 
Zgodnie z oryginalnym opisem wzorca, zarówno klasa Component jak i Composite zawiera metody operujące na komponentach podrzędnych, 
które są przechowywane w klasie Composite. Nowsze opisy umieszczają te metody tylko w klasie Composite.

Konsekwencje zastosowania:

Zalety:
 Umożliwia definiowanie hierarchii z obiektów prostych i złożonych, upraszcza kod klientów, ułatwia dodawanie komponentów nowego rodzaju
 
Wady:
 Może sprawić, że projekt stanie się zanadto ogólny
 
 źródło: https://pl.wikipedia.org/wiki/Kompozyt_(wzorzec_projektowy)