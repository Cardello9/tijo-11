import org.junit.jupiter.api.Test;

public class DepartmentTests {

    private Department headDepartment;
    private Department salesDepartment;
    private Department financialDepartment;

    @Test
    public void testDepartments() {
        Department salesDepartment = new SalesDepartment(1, "Sales department");
        Department financialDepartment = new FinancialDepartment(2, "Financial department");
        HeadDepartment headDepartment = new HeadDepartment(3, "Head department");
        headDepartment.addDepartment(salesDepartment);
        headDepartment.addDepartment(financialDepartment);
        headDepartment.printDepartmentName();
    }
}
